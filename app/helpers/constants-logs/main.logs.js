'use strict';

const Logs = require('@models/logs');

const findLogsByApplication = async (application_id) => {
    return Logs.find({ application_id });
}

module.exports = { findLogsByApplication };