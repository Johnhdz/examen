'use strict';

const { message200 } = require('@helpers/response-messages')

const responseStructure = async (code, message, payload) => {
    if (typeof payload === 'object') {
        const dataResult = {
            headerResponse: {
                code,
                message
            },
            payload
        };
        return dataResult;
    } else {
        const dataResult = {
            headerResponse: {
                code: 210,
                message: 'el cuerpo del payload no es un objeto'
            },
            payload
        };
        return dataResult;
    }
};

const responseValid = response => {
    const { headerResponse } = response;
    if (headerResponse.code !== 210) {
        return true;
    } else {
        return false;
    }
};

const requestValidate = async (schema, request, id) => {
    try {
        if (id) request.id = id;
        const validate = await schema.validateAsync(request);
        if (validate) return responseStructure(200, message200, {});
    } catch (err) {
        return responseStructure(401, err.details[0].message, {});
    }
};

module.exports = { 
    responseStructure, 
    responseValid, 
    requestValidate
};
