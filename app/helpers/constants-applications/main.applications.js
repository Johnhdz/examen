'use strict';

const Applications = require('@models/applications');

const findApplicationById = async (application_id) => {
    return Applications.findOne({ _id: application_id });
}

module.exports = { findApplicationById };