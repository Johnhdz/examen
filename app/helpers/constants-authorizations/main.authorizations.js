'use strict';

const jwt = require('jsonwebtoken');
const { responseStructure } = require('@helpers/constants');

function createToken() {
    const payload = {
        user: {
            name_application: process.env.NAME,
            app_key: process.env.APP_KEY
        }
    };

    return jwt.sign(payload, process.env.APP_KEY, { expiresIn: process.env.AUTH_TOKEN_TIME });
}

async function authRequired(req, res, next) {
    let token = req.headers['authorization'];
    if (!token) {
        return res.status(401).send(await responseStructure(401, 'Se requiere autenticación.', {}));
    }

    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
    }

    await jwt.verify(token, process.env.APP_KEY, async (error, decoded) => {
        if (error) {
            if (error.expiredAt) {
                return res.status(401).send(await responseStructure(401, `Token expirado`, {}));
            }
            return res.status(401).send(await responseStructure(401, `Error de autenticación:`, {}));
        }
        req.decoded = decoded;
        next();
    });
};

module.exports = { createToken, authRequired };
