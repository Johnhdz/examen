'use strict';

const message200 = 'Operacion realizada correctamente.';
const message400 = 'Peticion incorrecta.';
const message404 = 'No se encontraron resultados.';
const message500 = 'Ocurrio un problema en el servidor.';

module.exports = {
    message200,
    message400,
    message404,
    message500
}