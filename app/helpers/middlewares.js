'use strict';

const { responseStructure } = require('./constants');

exports.errorHandler = async (error, req, res, next) => {
    if (error.errmsg && error.errmsg.includes('E11000 duplicate key')) {
        const response = await responseStructure(400, 'Este registro ya existe en la base de datos, favor de verificar', {});
        return res.status(400).send(response);
    }
    if (error.name === 'ValidationError') {
        const params = error.errors.map(error => {
            return {
                campo: error.field[0],
                ubicación: error.location
            };
        });
        const response = await responseStructure(400, 'Petición no válida', { 'parametros requeridos/invalidos': params });
        return res.status(400).send(response);
    }

    const response = await responseStructure(500, error.message, error.data || {});
    return res.status(500).send(response);
};
