'use strict';

const mongoose = require('mongoose');
const { Schema } = mongoose;

const Applications = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true, trim: true, unique: true },
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Applications', Applications, 'applications');
