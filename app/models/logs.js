'use strict';

const mongoose = require('mongoose');
const { Schema } = mongoose;

const Logs = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    application_id: { type: Schema.Types.ObjectId, ref: 'Applications' },
    type: {
        type: String,
        enum: ['error', 'info', 'warning'],
        default: 'error'
    },
    priority: {
        type: String,
        enum: ['lowest', 'low', 'medium', 'high', 'highest'],
        default: 'lowest'
    },
    path: { type: String, required: true, trim: true },
    message: { type: String, required: true, trim: true },
    request: { type: Schema.Types.Mixed },
    response: { type: Schema.Types.Mixed },
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Logs', Logs, 'logs');
