'use strict';

const mongoose = require('mongoose');
const { Schema } = mongoose;

const Authorization = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    token: { type: String, required: true, trim: true },
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Authorization', Authorization, 'authorizations');
