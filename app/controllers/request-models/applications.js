'use strict';

const Joi = require('joi');

const applicationSchema = Joi.object({
    name: Joi.string()
        .min(3)
        .max(30)
        .required(),
});

const idApplicationSchema = Joi.object({
    id: Joi.string()
        .required(),
});

const updateApplicationSchema = Joi.object({
    id: Joi.string()
        .required(),
    name: Joi.string()
        .min(3)
        .max(30)
        .required(),
});

module.exports = { applicationSchema, updateApplicationSchema, idApplicationSchema };