'use strict';

const Joi = require('joi');

const logsSchema = Joi.object({
    application_id: Joi.string()
        .required(),
    type: Joi.string()
        .valid('error', 'info', 'warning')
        .required(),
    priority: Joi.string()
        .valid('lowest', 'low', 'medium', 'high', 'highest')
        .required(),
    path: Joi.string()
        .required(),
    message: Joi.string()
        .required(),
    request: Joi.any(),
    response: Joi.any()
});

const updateLogsSchema = Joi.object({
    id: Joi.string()
        .required(),
    application_id: Joi.string(),
    type: Joi.string()
        .valid('error', 'info', 'warning'),
    priority: Joi.string()
        .valid('lowest', 'low', 'medium', 'high', 'highest'),
    path: Joi.string(),
    message: Joi.string(),
    request: Joi.any(),
    response: Joi.any()
});

module.exports = { logsSchema, updateLogsSchema };