'use strict';

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Applications = require('@models/applications');
const { responseStructure, requestValidate } = require('@helpers/constants');
const { message200, message400, message404 } = require('@helpers/response-messages');
const { applicationSchema, updateApplicationSchema, idApplicationSchema } = require('../controllers/request-models/applications');
const { authRequired } = require('@helpers/constants-authorizations/main.authorizations');

router.post('/aggregate', authRequired, async (req, res, next) => {
    try {
        const validateRequest = await requestValidate(applicationSchema, req.body);
        if (validateRequest.headerResponse.code === 401) return res.status(401).send(validateRequest);
        
        req.body._id = mongoose.Types.ObjectId();
        const createApplication = new Applications(req.body);

        if (!createApplication) {
            return res.status(400).send(await responseStructure(400, message400, { }));
        }
        
        await createApplication.save();
        return res.status(200).send(await responseStructure(200, message200, createApplication));
    } catch (err) {
        next(err);
    }
});

router.get('/all', authRequired, async (req, res, next) => {
    try {
        const applications = await Applications.find();
        return res.status(200).send(await responseStructure(200, message200, applications));
    } catch (err) {
        next(err);
    }
});

router.get('/find-one/:id', authRequired, async (req, res, next) => {
    try {
        const validateRequest = await requestValidate(idApplicationSchema, {}, req.params.id);
        if (validateRequest.headerResponse.code === 401) return res.status(401).send(validateRequest);

        const application = await Applications.findOne({ _id: req.params.id });

        if (!application) return res.status(404).send(await responseStructure(404, message404, {}));
        return res.status(200).send(await responseStructure(200, message200, application));
    } catch (err) {
        next(err);
    }
});

router.put('/update/:id', authRequired, async (req, res, next) => {
    try {
        const { id } = req.params;
        const validateRequest = await requestValidate(updateApplicationSchema, req.body, id);
        if (validateRequest.headerResponse.code === 401) return res.status(401).send(validateRequest);

        const application = await Applications.findOne({ _id: id });
        if (!application) return res.status(404).send(await responseStructure(404, message404, {}));

        const update = await Applications.findOneAndUpdate({ _id: id }, req.body, { new: true });
        if (!update) return res.status(400).send(await responseStructure(400, message400, {}));
        return res.status(200).send(await responseStructure(200, message200, update));
    } catch (err) {
        next(err);
    }
});

module.exports = router;