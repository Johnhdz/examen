'use strict';

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Authorization = require('@models/authorizations');
const { responseStructure } = require('@helpers/constants');
const { message200, message400 } = require('@helpers/response-messages');
const { createToken } = require('../helpers/constants-authorizations/main.authorizations');

router.post('/sign-in', async (req, res, next) => {
    try {
        const id = mongoose.Types.ObjectId();
        const token = createToken();

        const createAuthorization = new Authorization({
            _id: id,
            token: token
        });
        
        if (!createAuthorization) {
            return res.status(400).send(await responseStructure(400, message400, {}));
        }
        
        await createAuthorization.save();
        return res.status(200).send(await responseStructure(200, message200, { token }));
    } catch (err) {
        next(err);
    }
});

module.exports = router;