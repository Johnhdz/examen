'use strict';

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Logs = require('@models/logs');
const { responseStructure, requestValidate } = require('@helpers/constants');
const { message200, message400, message404 } = require('@helpers/response-messages');
const { logsSchema, updateLogsSchema } = require('../controllers/request-models/logs');
const { findApplicationById } = require('../helpers/constants-applications/main.applications');
const { authRequired } = require('@helpers/constants-authorizations/main.authorizations');
const { idApplicationSchema } = require('../controllers/request-models/applications');

router.post('/aggregate', authRequired, async (req, res, next) => {
    try {
        const validateRequest = await requestValidate(logsSchema, req.body);
        if (validateRequest.headerResponse.code === 401) return res.status(401).send(validateRequest);
        
        const findAplication = await findApplicationById(req.body.application_id);
        if (!findAplication) return res.status(404).send(await responseStructure(404, message404, {}));

        req.body._id = mongoose.Types.ObjectId();
        const createLog = new Logs(req.body);

        if (!createLog) {
            return res.status(400).send(await responseStructure(400, message400, { }));
        }
        
        await createLog.save();
        return res.status(200).send(await responseStructure(200, message200, createLog));
    } catch (err) {
        next(err);
    }
});

router.get('/find-one/:id', authRequired, async (req, res, next) => {
    try {
        const validateRequest = await requestValidate(idApplicationSchema, {}, req.params.id);
        if (validateRequest.headerResponse.code === 401) return res.status(401).send(validateRequest);

        const log = await Logs.findOne({ _id: req.params.id});

        if (!log) return res.status(404).send(await responseStructure(404, message404, {}));
        return res.status(200).send(await responseStructure(200, message200, log));
    } catch (err) {
        next(err);
    }
});

router.get('/find-all/:application_id', authRequired, async (req, res, next) => {
    try {
        const logs = await Logs.aggregate([
            { $match : { application_id : ObjectId(req.params.application_id) } }
        ]);

        return res.status(200).send(await responseStructure(200, message200, logs));
    } catch (err) {
        next(err);
    }
});

router.put('/update/:id', authRequired, async (req, res, next) => {
    try {
        const { id } = req.params;
        const validateRequest = await requestValidate(updateLogsSchema, req.body, id);
        if (validateRequest.headerResponse.code === 401) return res.status(401).send(validateRequest);

        const log = await Logs.findOne({ _id: id });
        if (!log) return res.status(404).send(await responseStructure(404, message404, {}));

        const update = await Logs.findOneAndUpdate({ _id: id }, req.body, { new: true });
        if (!update) return res.status(400).send(await responseStructure(400, message400, {}));
        return res.status(200).send(await responseStructure(200, message200, update));
    } catch (err) {
        next(err);
    }
});

module.exports = router;