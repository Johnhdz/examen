'use strict';

const mongoose = require('mongoose');

mongoose.set("strictQuery", false);
const connect = mongoose.connect(`${process.env.DB_URI}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

module.exports = { connect }
