'use strict';

const router = require('express').Router();
const BASE_URL_APPLICATIONS = '/applications';
const BASE_URL_AUTHORIZATIONS = '/authorizations';
const BASE_URL_LOGS = '/logs';

const applications = require('@controllers/applications');
const authorizations = require('@controllers/authorizations');
const logs = require('@controllers/logs');

router.use(BASE_URL_APPLICATIONS, applications);
router.use(BASE_URL_AUTHORIZATIONS, authorizations);
router.use(BASE_URL_LOGS, logs);

module.exports = router;