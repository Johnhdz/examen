'use strict';

require('module-alias/register');

const express = require('express');
const app = express();

const cors = require('cors');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const logger = require('morgan');
const { errorHandler } = require('@helpers/middlewares');

const router = require('./routes/main.routes');
const path = require('path');

app.use(logger('dev'));
app.use(cors());
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', router);

app.use((req, res, next) => next(createError(404)));
app.use(errorHandler);

module.exports = app;
